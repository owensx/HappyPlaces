from django import forms


class UserSubmitForm(forms.Form):
    user_image = forms.ImageField()
