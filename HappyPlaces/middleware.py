from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin
from storages.backends.s3boto3 import S3Boto3Storage


class MediaStorage(S3Boto3Storage):
    bucket_name = 'happyplaces-submissions'


class HealthCheckMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.META["PATH_INFO"] == "/healthcheck":
            return HttpResponse("ready to serve")
